<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\UUIDO;

use Webmozart\Assert\Assert;

/**
 * Description of Ouid
 *
 * Questa classe genera indici sequenziali progressivi esadecimali di 16 cifre partendo dal timestamp della prima 
 * chiamata e utilizzando uncontatore che viene inizializzato in modo random.
 * Se viene fornito anche un esadecimale di nodo ... allora fornisce un id sequenziale di 32 cifre esadecimali.
 * 
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Maker implements \Iterator
{
    /**
     * namespace a cui appartengono i codici generati
     * @var hex
     */
    private $idNode;
    
    private $len;
    /**
     * Radice temporale degli id generati
     * @var hex
     */
    private $s_rpx = '';
    
    /**
     * Lista delle radici temporali già utilizzate
     * @var type 
     */
    private $s_lst = array();
    
    private $position = -1;
    private $current = null;
    
    private $counter;
    
    /**
     * I codici generati vengono assegnati ad un nodo
     * @param hex $idNode
     */
    public function __construct($idNode, $len = 32)
    {
        $this->idNode = check($idNode, strlen($idNode));
        $this->len = $len - strlen($idNode);
        $this->setCounter(new Counter());
        $this->init();
    }
    
    /**
     * Impostazione del contatore
     * @param Array $counter
     */
    public function setCounter(Counter $counter)
    {
        $this->counter = $counter;
    }
    
    public function getNode()
    {
        return $this->idNode;
    }
    public function getCounter()
    {
        return $this->counter;
    }
    /**
     * Inizializzazione componente
     */
    private function init()
    {
        $this->s_rpx = $this->getRadix();
        Assert::true(strlen($this->s_rpx) < $this->len, 'Lunghezza minima contatore insufficiente ['.$this->len.']: occorre piu\' di '.strlen($this->s_rpx).' caratteri');
        $this->counter->init(0, $this->s_rpx, $this->idNode);
    }
    
    /**
     * Quando il contatore ha raggiunto il max allora si cerca la radice successiva ... rallentando il processo di 
     * generazione
     * 
     * @return type
     */
    private function getRadix()
    {
        while (1) {
            $frm = explode(',',date('Y,z,G,i,s'));

            // Se Considerano gli anni come se il millennio corrente fosse il primo
            // La seconda parte viene composta con i numeri di secondi trascorsi da inizio anno.
            // Se l'anno è pari non si correggono ... se è dispari aggiungono i secondi di 500 giorni
            // In questo modo si mappano gli ID di 2 anni in una unica radice
            $mod = $frm[0] % 2;
            $div = ($frm[0] - 1800 - $mod) / 2;
            // numero secondi inizio anno
            $sec = $frm[1] * ($frm[2]*60*60 + $frm[3]*60 + $frm[4]);
            $sec += $mod ? (500 * 24 * 60 * 60) : 0;
            
            $rpx = $div . "" . str_pad($sec, 8,'0',STR_PAD_LEFT);
            // fino ache non trovo una radice valida 
            $radix = strtoupper(dechex($rpx));
            
            // viene inizializzato il contatore con 5 caratteri random
            list($uu,) = explode(' ',microtime());
            $uu *= 1000;
            // il numero da incrementare viene fatto iniziare da 5 numeri non negativi random
            $first = rand(10,99).str_pad($uu ? (int)$uu : rand(1,999), 3, '0', STR_PAD_LEFT);
            $radix .= str_pad(strtoupper(dechex($first)), 5, '0', STR_PAD_LEFT);
            
            if (!in_array($radix, $this->s_lst)) {
                // se non è stata ancora utilizzata ... si ferma la ricerca
                break;
            }
            // attesa prima di ricalcolare la radice con il timestamp
            usleep(100*1000);
        }
        $this->s_lst[] = $radix;
        // la stringa ha 9 caratteri
        return $radix;
    }
    public function current ()
    {
        return $this->current;
    }
    public function key ()
    {
        return $this->position;
    }
    public function rewind ()
    {
        if ($this->position > -1) {
            $this->init();
        }
        $this->position = -1;
        $this->current = null;
        $this->next();
    }
    public function valid ()
    {
        return true;
    }
    
    /**
     * Generatore di UUID sequenziali.
     * @param type $step
     * @return string
     */
    public function next()
    {
        // rappresentazione esadecimale del contatore
        Assert::true($this->len >= strlen($this->s_rpx), 'parte contatore non sufficientemente lunga. Deve essere almeno di '.strlen($this->s_rpx).' caratteri');
        $maxChar = $this->len - strlen($this->s_rpx);
        // al counter è affidato il compito di calcolare il valore successivo;
        $nextVal = $this->counter->next($this->s_rpx, $this->idNode);
        $value = str_pad(strtoupper(dechex($nextVal)), $maxChar, '0', STR_PAD_LEFT);
        // Se radice+valore ha lunghezza maggiore di len occorre reinizializzare la radice
        if (strlen($value) > $maxChar) {
            $this->init();
            return $this->next();
        }
        // il valore viene generato come concatenazione delle 2 stringhe esadecimali (10 caratteri + 6 caratteri)
        $value = $this->s_rpx . $value;
        $this->position += 1;
        $this->current = new UUIDO($value, $this->idNode, $this);
        return $this->current;
    }
}

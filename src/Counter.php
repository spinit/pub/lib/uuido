<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\UUIDO;

/**
 * Description of Counter
 * Contatore progressivo di default
 * 
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
use Spinit\Util;

class Counter
{
    private static $value = 0;
    private $initer = null;
    private $nexter = null;
    
    public function __construct($nexter = null, $initer = null)
    {
        $this->initer = Util\nvl($initer, function() {
            $args = func_get_args();
            self::$value = (int) array_shift($args);
        });
        $this->nexter = Util\nvl($nexter, function() {
            ++self::$value;
            return self::$value;
        });
    }
    public function init()
    {
        $args = func_get_args();
        call_user_func_array($this->initer, $args);
    }
    public function next()
    {
        $args = func_get_args();
        return call_user_func_array($this->nexter, $args);
    }
}

<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\UUIDO;

/**
 * Elemento che identifica un uuido 
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class UUIDO implements \JsonSerializable
{
    private $node;
    private $counter;
    private $maker;
    
    public function __construct($counter, $node, $maker = null)
    {
        $this->counter = check($counter);
        $this->node = check($node);
        $this->maker = $maker;
    }
    
    public function getNode()
    {
        return $this->node;
    }
    
    public function getCounter()
    {
        return $this->counter;
    }
    
    /**
     * La rappresentazione è data dalla concatenazione del valore con la rappresentazione del nodo
     * @return type
     */
    public function __toString()
    {
        return $this->getCounter() . $this->getNode();
    }
    
    /**
     * Se viene passato il generatore ... allora lo interroga per richiedere il successivo
     * @return type
     */
    public function next()
    {
        if ($this->maker) {
            $this->maker->next();
            return $this->maker->current();
        }
        return null;
    }

    public function jsonSerialize()
    {
        return (string) $this;
    }

}
